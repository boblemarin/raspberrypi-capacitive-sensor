#!/user/bin/env python

#leds = [21,22,23,24,25]
#for i in leds:
#    print i

import RPi.GPIO as GPIO, time, os

GPIO.setmode(GPIO.BCM)
#GPIO.setwarnings(False)

# init LEDs sequence
leds = [27,22,23,24,25,10,8,7]

for i in leds:
    GPIO.setup( i,GPIO.OUT )
    GPIO.output( i,GPIO.HIGH )

time.sleep(2)

GPIO.cleanup()